# <a href="https://github.com/rhildred/ES6OrderBot" target="_blank">ES6 Order Bot</a>

I got the user interface for the web from a student of mine, Pat Wilken.

To run:

1. Sign up for paypal developer sandbox and get a client id
2. The first time run `npm install`
3. SB_CLIENT_ID="<Put your client_id here without>" npm start
   OR Hard code the SB_CLIENT_ID in assignment1.Pizza.js file.
   Search for 'HARDCODE YOUR CLIENTID HERE' and replace.

## Assignment 1

Create an order bot for your favourite food. You need to have at least 2 items on the menu unless your favourite food is pizza in which case you need to have 3 (I did pizza already). The menu items need to have size and one other attribute like toppings. You also need an up-sell item like drinks in the example.

### Marking

basic order for an item in a zip (65%)
2nd or 3rd item (up to 10%)
up-sell item other than drinks (up to 10%)
estimated price or ability for someone in the store to see the orders with their phone (up to 15%)

There is a brief [presentation here](EventsAndObjects.pdf).

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

## Build install and use the project
You can clone or download the project. Once you have the project on you local machine then:
1. Open VS code terminal.
2. Run command npm install.
3. Run command node index.js.
4. Open browser and the project runs on http://localhost:3002.

## License and copyright
© ADITYA GUPTA, Conestoga College

Licensed under the [MIT License](LICENSE). 

Licensed under the [MIT License](LICENSE). 