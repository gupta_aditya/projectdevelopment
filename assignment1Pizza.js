const Order = require("./assignment1Order");

const OrderState = Object.freeze({
    WELCOMING: Symbol("welcoming"),
    SIZE: Symbol("size"),
    TOPPINGS: Symbol("toppings"),
    ADDPIZZA: Symbol("addpizza"),
    ADDSANDWICH: Symbol("addsandwich"),
    BILL: Symbol("bill"),
    SANDWICHCOMBO: Symbol("sandwichcombo"),
    ADDITEMS: Symbol("additems"),
    PAYMENT: Symbol("payment")
});

const pizzaSmPrice = 20;
const pizzaMdPrice = 30;
const pizzaLgPrice = 40;
const sandwichPrice = 15;
const sandwichComboPrice = 30;
const toppingPepproni = 2;
const toppingGreenPepper = 1;
const toppingOthers = .5;

module.exports = class ShwarmaOrder extends Order {
    constructor(sNumber, sUrl) {
        super(sNumber, sUrl);
        this.stateCur = OrderState.WELCOMING;
        this.pizzaSize = "";
        this.sToppings = "";
        this.sItem = "pizza";
        this.welcomeInput = "";
        this.orderTotal = 0;
        this.isSandwich = false;
        this.isSandwichCombo = false;
    }
    handleInput(sInput) {
        let aReturn = [];
        switch (this.stateCur) {
            case OrderState.WELCOMING:
                aReturn.push("Welcome to Sam's Pizza Hut. How can I help you today?");
                aReturn.push("Press 1 to make a new order");
                aReturn.push("Press 2 to track your order");
                this.stateCur = OrderState.ADDITEMS;
                break;
            case OrderState.ADDITEMS:
                if (sInput === "1") {
                    this.stateCur = OrderState.ADDPIZZA;
                    aReturn.push("What size of Pizza would you like?");
                    aReturn.push("Press 1 for small pizza");
                    aReturn.push("Press 2 for medium pizza");
                    aReturn.push("Press 3 for large pizza");
                } else if (sInput === "2") {
                    aReturn.push("Sorry, tracking order feauture is under site development.");
                    aReturn.push("Press 1 to make a new order");
                    this.stateCur = OrderState.ADDITEMS;
                } else {
                    aReturn.push("Sorry i didn't get that. Can you say that again ?");
                    aReturn.push("Press 1 to make a new order");
                    this.stateCur = OrderState.ADDITEMS;
                }
                break;
            case OrderState.ADDPIZZA:
                this.stateCur = OrderState.TOPPINGS
                if (sInput === "1") {
                    this.pizzaSize = "small";
                    this.orderTotal += pizzaSmPrice;
                } else if (sInput === "2") {
                    this.pizzaSize = "medium";
                    this.orderTotal += pizzaMdPrice;
                } else if (sInput === "3") {
                    this.pizzaSize = "large";
                    this.orderTotal += pizzaLgPrice;
                } else {
                    aReturn.push("Sorry i didn't get that. Can you say that again ?");
                    aReturn.push("Press 1 for small pizza");
                    aReturn.push("Press 2 for medium pizza");
                    aReturn.push("Press 3 for large pizza");
                    this.stateCur = OrderState.ADDPIZZA;
                }
                if (this.stateCur === OrderState.TOPPINGS) {
                    aReturn.push("What toppings would you like ?");
                    aReturn.push("Press 1 for Pepperoni");
                    aReturn.push("Press 2 for Green Peppers");
                    aReturn.push("Or, type in your toppings if not listed.");
                }
                break;
            case OrderState.TOPPINGS:
                if (sInput == "1") {
                    this.sToppings = "pepproni";
                    this.orderTotal += toppingPepproni;
                } else if (sInput == "2") {
                    this.sToppings = "green pepper";
                    this.orderTotal += toppingGreenPepper;
                } else {
                    this.sToppings = sInput;
                    this.orderTotal += toppingOthers;
                }
                this.stateCur = OrderState.ADDSANDWICH;
                aReturn.push("Your desired toppings has been added");
                aReturn.push(`Would you like to have Chicken Sandwich aswell just for ${sandwichPrice}CAD?`);
                aReturn.push("Press 1 for YES");
                aReturn.push("Press 2 for NO");
                break;
            case OrderState.ADDSANDWICH:
                if (sInput === "1") {
                    this.isSandwich = true;
                    this.orderTotal += sandwichPrice;
                    this.stateCur = OrderState.SANDWICHCOMBO;
                    aReturn.push("Would you like a upgrade to a chicken sandwich combo with Poutine and 500ml drinks?");
                    aReturn.push("Press 1 for YES");
                    aReturn.push("Press 2 for NO");
                } else if (sInput === "2") {
                    aReturn.push("Press 1 to bill your order");
                    this.stateCur = OrderState.BILL;
                } else {
                    aReturn.push("Sorry i didn't get that. Can you say that again ?");
                    aReturn.push("Press 1 for YES");
                    aReturn.push("Press 2 for NO");
                    this.stateCur = OrderState.ADDSANDWICH;
                }
                break;
            case OrderState.SANDWICHCOMBO:
                if (sInput === "1") {
                    this.isSandwichCombo = true;
                    this.isSandwich = false;
                    this.orderTotal = this.orderTotal + sandwichComboPrice - sandwichPrice;
                    this.stateCur = OrderState.BILL;
                    aReturn.push("Press 1 to bill your order");
                } else if (sInput === "2") {
                    this.stateCur = OrderState.BILL;
                    aReturn.push("Press 1 to bill your order");
                } else {
                    aReturn.push("Sorry i didn't get that. Can you say that again ?");
                    aReturn.push("Press 1 for YES");
                    aReturn.push("Press 2 for NO");
                    this.stateCur = OrderState.SANDWICHCOMBO;
                }
                break;
            case OrderState.BILL:
                this.isDone(true);
                this.stateCur = OrderState.PAYMENT;
                aReturn.push("Thank-you for your order of");
                aReturn.push(`${this.pizzaSize} ${this.sItem} with ${this.sToppings}`);
                if (this.isSandwich) {
                    aReturn.push(`And a chicken sandwich`);
                }
                if (this.isSandwichCombo) {
                    aReturn.push(`And a chicken sandwich combo with poutine and 500ml drinks`);
                }
                this.nOrder = this.orderTotal;
                aReturn.push(`Your estimated order total excluding taxes will be ${this.orderTotal} CAD`);
                aReturn.push(`Tax @13% will be ${this.orderTotal * .13} CAD`);
                aReturn.push(`Please pay for your order here`);
                aReturn.push(`${this.sUrl}/payment/${this.sNumber}/`);
                break;
            case OrderState.PAYMENT:
                this.isDone(true);
                let d = new Date();
                d.setMinutes(d.getMinutes() + 20);
                var adressObj = sInput.purchase_units[0].shipping.address;
                aReturn.push(`Thankyou, your payment is successfull !!`);
                aReturn.push(`Your order will be delivered by ${d.toTimeString()}`);
                aReturn.push(`At Address`);
                aReturn.push(`${adressObj.address_line_1}`);
                aReturn.push(`${adressObj.admin_area_2} ${adressObj.admin_area_1} ${adressObj.postal_code} ${adressObj.country_code}`);
                break;
        }
        return aReturn;
    }
    renderForm(sTitle = "-1", sAmount = "-1") {
        // your client id should be kept private
        if (sTitle != "-1") {
            this.sItem = sTitle;
        }
        if (sAmount != "-1") {
            this.nOrder = sAmount;
        }
        const sClientID = process.env.SB_CLIENT_ID || 'Aa6vc44bsC_hICa5b2imTrKaEJYeqRsXTXG_nQs9X4m0MMx7Mn9iRP7CB8sRiWOlKbiSkPVSdf0rEwKe';
        return (`
        <!DOCTYPE html>
    
        <head>
          <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices. -->
          <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->
        </head>
        
        <body>
          <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
          <script
            src="https://www.paypal.com/sdk/js?client-id=${sClientID}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
          </script>
          Thank you ${this.sNumber} for your ${this.sItem} order of $${this.nOrder}.
          <div id="paypal-button-container"></div>
          <script src="/js/order.js" type="module"></script>
          <script>
            paypal.Buttons({
                createOrder: function(data, actions) {
                  // This function sets up the details of the transaction, including the amount and line item details.
                  return actions.order.create({
                    purchase_units: [{
                      amount: {
                        value: '${this.nOrder}'
                      }
                    }]
                  });
                },
                onApprove: function(data, actions) {
                    // This function captures the funds from the transaction.
                    return actions.order.capture().then(function(details) {
                      console.log('Transaction approved by ' + details);
                      // This function shows a transaction success message to your buyer.
                      
                      $.post(".", details, ()=>{
                        details.order = ${JSON.stringify(this)};
                        window.fSaveOrder(details);
                      });
                    });
                  }
            
              }).render('#paypal-button-container');
            // This function displays Smart Payment Buttons on your web page.
          </script>
        
        </body>
            
        `);

    }
}